a=`head -n 1 serverinfo.dinfo`
b=`tail -n+3 serverinfo.dinfo`
c="        server_name ${b};"
echo 'server {' > /etc/nginx/sites-enabled/$a
echo $c>> /etc/nginx/sites-enabled/$a
echo '        recursive_error_pages on;'>> /etc/nginx/sites-enabled/$a
echo ''>> /etc/nginx/sites-enabled/$a
echo '        location / {'>> /etc/nginx/sites-enabled/$a
echo '            proxy_pass http://localhost:3000;'>> /etc/nginx/sites-enabled/$a
echo '            proxy_http_version 1.1;'>> /etc/nginx/sites-enabled/$a
echo '            proxy_set_header Upgrade $http_upgrade;'>> /etc/nginx/sites-enabled/$a
echo '            proxy_set_header Connection "upgrade";'>> /etc/nginx/sites-enabled/$a
echo '            proxy_set_header Host $host;'>> /etc/nginx/sites-enabled/$a
echo '            proxy_cache_bypass $http_upgrade;'>> /etc/nginx/sites-enabled/$a
echo '        }'>> /etc/nginx/sites-enabled/$a
echo '       '>> /etc/nginx/sites-enabled/$a
echo '        location ~ /.well-known/acme-challenge {'>> /etc/nginx/sites-enabled/$a
echo '            allow all;'>> /etc/nginx/sites-enabled/$a
echo '            root /var/www/scan/.well-known/acme-challenge;'>> /etc/nginx/sites-enabled/$a
echo '        }'>> /etc/nginx/sites-enabled/$a
echo ''>> /etc/nginx/sites-enabled/$a
echo ''>> /etc/nginx/sites-enabled/$a
echo '}'>> /etc/nginx/sites-enabled/$a
nginx -t &&
service nginx restart
