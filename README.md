# NodeJS шаблон сервера

###### 1. Технические требования

> Ubuntu 16.04 x64 или выше

> Последние стабильные NPM & NODEJS 

> Последний стабильный MySQL

> 10Гб HDD, 2 Гб RAM, 2 core CPU

> Внесённая DNS запись типа А

###### 2. Подготовка сервера

```
sudo apt-get update && sudo apt-get upgrade -y && \
sudo apt-get install curl -y && curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash - && \
sudo apt-get update && sudo apt-get install nodejs nginx mysql-server -y && \
sudo mysql_secure_installation && apt-get install git -y && \
sudo add-apt-repository ppa:certbot/certbot -y && sudo apt-get update && \
sudo apt install python-certbot-nginx -y
```

###### 3. Подготовка git окружения
Используется переменная name и domain. В дальнейшнем они будут использоваться для настроек nginx и получения сертификата
```
read -p 'Имя проекта (имя сервера): ' name && \
read -p 'Домен проекта: ' domain && \
mkdir /var/www/$name && cd / && \
mkdir /temp && cd /temp && \
git clone https://gitlab.com/dzord/template.git && \
cp -R /temp/template/* /var/www/$name/ && \
cd /var/www/$name && rm -rf /temp/template && \
rm -rf /temp && \
echo 'Полная dns запись: ' $name.$domain && \
echo $name > serverinfo.dinfo && \
echo $domain >> serverinfo.dinfo && \
echo $name.$domain >> serverinfo.dinfo && \
mkdir .well-known && \
mkdir .well-known/acme-challenge && \
npm i -g pm2 && npm install express --save && \
npm i express-generator -g && \
npm i http-errors
```

###### 4. Подготовка nginx
Используется переменная name && domain из временного файла serverinfo.dinfo
Скрипт создаст файл с конфигом и применит его исходя из данных пункта 3

> Настройка nginx произведется самостоятельно

```
bash nginx-configure.sh
```
###### 5. Установка letsencrypt сертификата
Используется переменная name && domain из временного файла serverinfo.dinfo
Скрипт получит сертификат и включит автополучение

> Перед запуском проверить прописана ли dsn запись

```
pm2 start app.js && bash letsencrypt-configure.sh
```

###### 6. Создание окружения NodeJS

```
npm init 
```

###### 7. Использование

```
pm2 reload app.js
```

###### 8. Установка phpmyadmin

```
sudo apt-get install software-properties-common && \
sudo add-apt-repository ppa:ondrej/php && \
sudo apt-get update && \
sudo apt install php7.2-fpm php7.2-common php7.2-mbstring php7.2-xmlrpc php7.2-soap php7.2-gd php7.2-xml php7.2-intl php7.2-mysql php7.2-cli php7.2-zip php7.2-curl
```

###### 9. Настройка phpmyadmin

```
sudo nano /etc/php/7.2/fpm/php.ini
```
```
file_uploads = On
allow_url_fopen = On
memory_limit = 256M
upload_max_filesize = 100M
max_execution_time = 360
date.timezone = Europe/Moscow
```

Редактировние конфига
```
rm -rf /etc/nginx/sites-enabled/default && \
nano /etc/nginx/snippets/phpmyadmin.conf
```
```
location /phpmyadmin {
    root /usr/share/;
    index index.php index.html index.htm;
    location ~ ^/phpmyadmin/(.+\.php)$ {
        try_files $uri =404;
        root /usr/share/;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include /etc/nginx/fastcgi_params;
    }

    location ~* ^/phpmyadmin/(.+\.(jpg|jpeg|gif|css|png|js|ico|html|xml|txt))$ {
        root /usr/share/;
    }
}

```
Перезапускаем службы
```
sudo systemctl restart php7.2-fpm && \
sudo systemctl restart nginx && \
sudo apt install phpmyadmin && \
sudo ln -s /etc/nginx/sites-available/example.com.conf /etc/nginx/sites-enabled/
sudo systemctl restart nginx.service
sudo systemctl restart php7.2-fpm.service
```

###### 10. Подготовка phpmyadmin

```

# Добавить в конфигурацию 2 строчки в корне server 
include snippets/phpmyadmin.conf;
sudo systemctl restart nginx.service
```

###### 11. Подготовка sequelize

```
npm install --save sequelize && \
npm install --save mysql2 && \
npm install --save-dev sequelize-cli && \
npx sequelize init
```
